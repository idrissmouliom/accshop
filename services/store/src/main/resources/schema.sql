CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE SEQUENCE customer_id_seq START 1;

CREATE TABLE customer (
    customer_id BIGINT DEFAULT nextval('customer_id_seq') PRIMARY KEY,
    customer_code VARCHAR(50) UNIQUE DEFAULT ('CUST-' || uuid_generate_v4()::TEXT),
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(50) NOT NULL,
    phone_number VARCHAR(20),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    metadata JSONB
    );

CREATE SEQUENCE address_id_seq START 1;
CREATE TABLE address(
    address_id BIGINT DEFAULT nextval('address_id_seq') PRIMARY KEY,
    address_code VARCHAR(50) UNIQUE DEFAULT ('ADDR-' || uuid_generate_v4()::TEXT),
    customer_code VARCHAR(50) NOT NULL,
    street VARCHAR(100) NOT NULL,
    city VARCHAR(100) NOT NULL,
    zipcode VARCHAR(50) NOT NULL,
    country VARCHAR(50) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    metadata JSONB,
    CONSTRAINT fk_customer_code FOREIGN KEY (customer_code) REFERENCES customer(customer_code)
);


-- Create the product table with the necessary columns
CREATE SEQUENCE product_id_seq START 1;

CREATE TABLE product (
    product_id BIGINT DEFAULT nextval('product_id_seq') PRIMARY KEY,
    product_code VARCHAR(50) UNIQUE DEFAULT ('PROD-' || uuid_generate_v4()::TEXT) NOT NULL,
    name VARCHAR(100),
    description TEXT,
    price DOUBLE PRECISION,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    metadata JSONB,
    CONSTRAINT chk_price CHECK (price >= 0)
);

CREATE SEQUENCE cart_id_seq START 1;
CREATE TABLE cart(
    cart_id BIGINT DEFAULT nextval('cart_id_seq') PRIMARY KEY,
    cart_code VARCHAR(50) UNIQUE DEFAULT ('CART-' || uuid_generate_v4()::TEXT),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    cart_items_count DECIMAL DEFAULT 0,
    total_price DECIMAL(10,2) DEFAULT 0.00,
    shipping_method VARCHAR(50),
    shipping_cost DECIMAL(10,2) DEFAULT 0.00,
    currency VARCHAR(20) DEFAULT 'EUR',
    expiry_date TIMESTAMP,
    metadata JSONB,
    CONSTRAINT fk_customer_code FOREIGN KEY (customer_code) REFERENCES customer(customer_code),
    CONSTRAINT chk_currency CHECK (currency IN ('EUR','USD','GBP','CHF','YEN','CNY'))
);

CREATE SEQUENCE purchase_id_seq START 1;

CREATE TABLE purchase (
    purchase_id BIGINT DEFAULT nextval('purchase_id_seq') PRIMARY KEY,
    purchase_code VARCHAR(50) UNIQUE DEFAULT ('PURC-' || uuid_generate_v4()::TEXT),
    customer_code VARCHAR(50) NOT NULL,
    billing_address_code VARCHAR(50) NOT NULL,
    shipping_address_code VARCHAR(50) NOT NULL,
    cart_code VARCHAR(50) UNIQUE NOT NULL, -- UNIQUE constraint for one-to-one relationship
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    purchase_status VARCHAR(50),
    payment_status VARCHAR(50),
    metadata JSONB,
    CONSTRAINT fk_customer_code FOREIGN KEY (customer_code) REFERENCES customer(customer_code),
    CONSTRAINT fk_billing_address_code FOREIGN KEY (billing_address_code) REFERENCES address(address_code),
    CONSTRAINT fk_shipping_address_code FOREIGN KEY (shipping_address_code) REFERENCES address(address_code),
    CONSTRAINT fk_cart_code FOREIGN KEY (cart_code) REFERENCES cart(cart_code)
);

CREATE SEQUENCE cart_item_id_seq START 1;
CREATE TABLE cart_item(
    cart_item_id BIGINT DEFAULT nextval('cart_item_id_seq') PRIMARY KEY,
    cart_item_code VARCHAR(50) UNIQUE DEFAULT ('CITE-' || uuid_generate_v4()::TEXT),
    cart_code VARCHAR(50) NOT NULL,
    product_code VARCHAR(50) NOT NULL,
    quantity INT NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    shipping_cost DECIMAL(10,2) DEFAULT 0.00,
    metadata JSONB,
    CONSTRAINT fk_cart_code FOREIGN KEY (cart_code) REFERENCES cart(cart_code),
    CONSTRAINT fk_product_code FOREIGN KEY (product_code) REFERENCES product(product_code)
);

CREATE SEQUENCE inventory_id_seq START 1;
CREATE TABLE inventory(
    inventory_id BIGINT DEFAULT nextval('inventory_id_seq') PRIMARY KEY,
    product_code VARCHAR(50) NOT NULL,
    quantity INT NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_product_code FOREIGN KEY (product_code) REFERENCES product(product_code)
);

CREATE SEQUENCE category_id_seq START 1;
CREATE TABLE category(
    category_id BIGINT DEFAULT nextval('category_id_seq') PRIMARY KEY,
    category_code VARCHAR(50) UNIQUE DEFAULT ('CATE-' || uuid_generate_v4()::TEXT),
    name VARCHAR(50) NOT NULL,
    description TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE product_category(
    PRIMARY KEY (product_code,category_code),
    product_code VARCHAR(50) NOT NULL,
    category_code VARCHAR(50) NOT NULL,
    CONSTRAINT fk_product_code FOREIGN KEY (product_code) REFERENCES product(product_code),
    CONSTRAINT fk_category_code FOREIGN KEY (category_code) REFERENCES category(category_code)
);