package com.accshop.store.service;

import com.accshop.store.dto.PaymentDTO;
import com.accshop.store.dto.PurchaseDTO;
import com.accshop.store.entity.Address;
import com.accshop.store.entity.Cart;
import com.accshop.store.entity.Customer;
import com.accshop.store.entity.Purchase;
import com.accshop.store.enums.PaymentStatus;
import com.accshop.store.mapper.PurchaseMapper;
import com.accshop.store.repository.AddressRepository;
import com.accshop.store.repository.CartRepository;
import com.accshop.store.repository.CustomerRepository;
import com.accshop.store.repository.PurchaseRepository;
import jakarta.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class PurchaseService {

    private final RestTemplate restTemplate;
    private final PurchaseRepository purchaseRepository;
    private final PurchaseMapper purchaseMapper;
    private final CustomerRepository customerRepository;
    private final AddressRepository addressRepository;
    private final CartRepository cartRepository;
    private static final Logger logger = LoggerFactory.getLogger(PurchaseService.class);

    @Value("${payment.service.url}")
    private String paymentServiceUrl;

    public PurchaseService(RestTemplate restTemplate, PurchaseRepository purchaseRepository, PurchaseMapper purchaseMapper, CustomerRepository customerRepository, AddressRepository addressRepository, CartRepository cartRepository) {
        this.restTemplate = restTemplate;
        this.purchaseRepository = purchaseRepository;
        this.purchaseMapper = purchaseMapper;
        this.customerRepository = customerRepository;
        this.addressRepository = addressRepository;
        this.cartRepository = cartRepository;
    }

    public PurchaseDTO get(int id) {
        Optional<Purchase> optionalPurchase = this.purchaseRepository.findById(id);
        return optionalPurchase.map(this.purchaseMapper::toDTO).orElse(null);
    }

    public PurchaseDTO create(PurchaseDTO purchaseDTO) {
        // Retrieve Entity or throw an exception if not found
        Customer customer = this.customerRepository.findByCustomerCode(purchaseDTO.customerCode())
                .orElseThrow(() -> new EntityNotFoundException("Customer not found with code: " + purchaseDTO.customerCode()));

        Address billingAddress = this.addressRepository.findByAddressCode(purchaseDTO.billingAddressCode())
                .orElseThrow(() -> new EntityNotFoundException("Billing address not found with code: " + purchaseDTO.billingAddressCode()));

        Address shippingAddress = this.addressRepository.findByAddressCode(purchaseDTO.shippingAddressCode())
                .orElseThrow(() -> new EntityNotFoundException("Shipping address not found with code: " + purchaseDTO.shippingAddressCode()));

        Cart cart = this.cartRepository.findByCartCode(purchaseDTO.cartCode())
                .orElseThrow(() -> new EntityNotFoundException("Cart not found with code: " + purchaseDTO.cartCode()));

        // Map the PurchaseDTO to a Purchase entity
        Purchase purchase = this.purchaseMapper.toPurchase(purchaseDTO, customer, billingAddress, shippingAddress, cart);

        // Prepare the PaymentDTO for the payment service
        // Prepare the PaymentDTO for the payment service
        PaymentDTO paymentDTO = new PaymentDTO(
                null, // paymentId, if it is not set yet
                null, // paymentCode, if it is generated in the payment service
                purchaseDTO.purchaseCode(),
                purchaseDTO.customerCode(),
                null, // paymentStatus, can be set later based on service response
                cart.getTotalPrice(), // Assuming totalPrice is the amount
                null, // transactionId, if not available yet
                null, // createdAt, if set by the payment service
                null, // updatedAt, if set by the payment service
                null  // metadata, if not needed initially
        );

        try{
            String url = paymentServiceUrl;
            ResponseEntity<PaymentDTO> response= restTemplate.postForEntity(url,paymentDTO,PaymentDTO.class);
            PaymentDTO createdPayment = response.getBody();

            if(createdPayment != null && createdPayment.paymentId()!= null){
                purchase.setPaymentStatus(PaymentStatus.COMPLETED);
            }else {
                purchase.setPaymentStatus(PaymentStatus.FAILED);
            }

        }catch (Exception e){
            purchase.setPaymentStatus(PaymentStatus.FAILED);
        }
        // Save the purchase entity to the repository
        this.purchaseRepository.save(purchase);

        // Return the PurchaseDTO (assuming you map it back from the Purchase entity)
        return this.purchaseMapper.toDTO(purchase);
    }

   public PaymentDTO createPayment(PaymentDTO paymentDTO){
        String url = paymentServiceUrl;
        ResponseEntity<PaymentDTO> response = restTemplate.postForEntity(url,paymentDTO, PaymentDTO.class);
        return response.getBody();
   }


   @Async
   public CompletableFuture<PaymentDTO> createPaymentAsync(PaymentDTO paymentDTO){
        String url = paymentServiceUrl;
        ResponseEntity<PaymentDTO> response = restTemplate.postForEntity(url, paymentDTO,PaymentDTO.class);
        return CompletableFuture.completedFuture(response.getBody());
   }

   @Async
   public CompletableFuture<PaymentDTO> getPaymentByPurchaseCodeAsync(String purchaseCode){
        String url = paymentServiceUrl + "/" + purchaseCode;
        ResponseEntity<PaymentDTO> response = restTemplate.getForEntity(url,PaymentDTO.class);
        return CompletableFuture.completedFuture(response.getBody());
  }

  public PurchaseDTO getPurchaseByCode(String purchaseCode) {
        Optional<Purchase> optionalPurchase = this.purchaseRepository.findByPurchaseCode(purchaseCode);
        return optionalPurchase.map(this.purchaseMapper::toDTO).orElse(null);
  }

  public PurchaseDTO update(int id,PurchaseDTO purchaseDTO) {
        Purchase purchaseInDB = this.purchaseRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Purchase not found with id:" + id) );

        // Fetch and set the customer based on customerCode from PurchaseDTO
        Customer customer = this.customerRepository.findByCustomerCode(purchaseDTO.customerCode())
                .orElseThrow(() -> new EntityNotFoundException("Customer not found with code: " + purchaseDTO.customerCode()));
        purchaseInDB.setCustomer(customer);

        // Fetch and set the billing address based on billingAddressCode from PurchaseDTO
        Address billingAddress = this.addressRepository.findByAddressCode(purchaseDTO.billingAddressCode())
                .orElseThrow(() -> new EntityNotFoundException("Billing address not found with code: " + purchaseDTO.billingAddressCode()));
        purchaseInDB.setBillingAddress(billingAddress);

        // Fetch and set the shipping address based on shippingAddressCode from PurchaseDTO
        Address shippingAddress = this.addressRepository.findByAddressCode(purchaseDTO.shippingAddressCode())
                .orElseThrow(() -> new EntityNotFoundException("Shipping address not found with code: " + purchaseDTO.shippingAddressCode()));
        purchaseInDB.setShippingAddress(shippingAddress);

        // Fetch and set the cart based on cartCode from PurchaseDTO
        Cart cart = this.cartRepository.findByCartCode(purchaseDTO.cartCode())
                .orElseThrow(() -> new EntityNotFoundException("Cart not found with code: " + purchaseDTO.cartCode()));
        purchaseInDB.setCart(cart);

        // Update other fields
        purchaseInDB.setPurchaseStatus(purchaseDTO.purchaseStatus());
        purchaseInDB.setPaymentStatus(purchaseDTO.paymentStatus());
        purchaseInDB.setMetadata(purchaseDTO.metadata());

        // Save the updated Purchase entity
        Purchase updatedPurchase = this.purchaseRepository.save(purchaseInDB);

        // Map the updated Purchase entity back to PurchaseDTO
        return this.purchaseMapper.toDTO(updatedPurchase);
    }

    public PurchaseDTO updateByCode(String purchaseCode,PurchaseDTO purchaseDTO) {
        Purchase purchaseInDB = this.purchaseRepository.findByPurchaseCode(purchaseCode).orElseThrow(() -> new EntityNotFoundException("Purchase not found with code:" + purchaseCode) );

        // Fetch and set the customer based on customerCode from PurchaseDTO
        Customer customer = this.customerRepository.findByCustomerCode(purchaseDTO.customerCode())
                .orElseThrow(() -> new EntityNotFoundException("Customer not found with code: " + purchaseDTO.customerCode()));
        purchaseInDB.setCustomer(customer);

        // Fetch and set the billing address based on billingAddressCode from PurchaseDTO
        Address billingAddress = this.addressRepository.findByAddressCode(purchaseDTO.billingAddressCode())
                .orElseThrow(() -> new EntityNotFoundException("Billing address not found with code: " + purchaseDTO.billingAddressCode()));
        purchaseInDB.setBillingAddress(billingAddress);

        // Fetch and set the shipping address based on shippingAddressCode from PurchaseDTO
        Address shippingAddress = this.addressRepository.findByAddressCode(purchaseDTO.shippingAddressCode())
                .orElseThrow(() -> new EntityNotFoundException("Shipping address not found with code: " + purchaseDTO.shippingAddressCode()));
        purchaseInDB.setShippingAddress(shippingAddress);

        // Fetch and set the cart based on cartCode from PurchaseDTO
        Cart cart = this.cartRepository.findByCartCode(purchaseDTO.cartCode())
                .orElseThrow(() -> new EntityNotFoundException("Cart not found with code: " + purchaseDTO.cartCode()));
        purchaseInDB.setCart(cart);

        // Update other fields
        purchaseInDB.setPurchaseStatus(purchaseDTO.purchaseStatus());
        purchaseInDB.setPaymentStatus(purchaseDTO.paymentStatus());
        purchaseInDB.setMetadata(purchaseDTO.metadata());

        // Save the updated Purchase entity
        Purchase updatedPurchase = this.purchaseRepository.save(purchaseInDB);

        // Map the updated Purchase entity back to PurchaseDTO
        return this.purchaseMapper.toDTO(updatedPurchase);
    }

    public void delete(int id) {
        this.purchaseRepository.deleteById(id);
    }

    public void deleteByCode(String purchaseCode) {
        this.purchaseRepository.deleteByPurchaseCode(purchaseCode);
    }
}
