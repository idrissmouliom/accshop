package com.accshop.store.service;

import com.accshop.store.dto.ProductDTO;
import com.accshop.store.entity.Product;
import com.accshop.store.mapper.ProductMapper;
import com.accshop.store.repository.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductService(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public ProductDTO create(ProductDTO productDTO) {
        Product product = this.productMapper.toProduct(productDTO);
        Product savedProduct = this.productRepository.save(product);

        return this.productMapper.toDTO(savedProduct);
    }

    public Product get(int id) {
        Optional<Product> optionalProduct = this.productRepository.findById(id);
        return optionalProduct.orElse(null);
    }

    public ProductDTO getProductByCode(String productCode) {
        Product productInDB = this.productRepository.findByProductCode(productCode).orElseThrow(
                () -> new EntityNotFoundException("Product not found with code: " + productCode));

        return this.productMapper.toDTO(productInDB);
    }

    public ProductDTO update(int id, ProductDTO productDTO) {
        Product productInDB = this.productRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Product not found with id: " + id ));

        productInDB.setName(productDTO.name());
        productInDB.setPrice(productDTO.price());
        productInDB.setDescription(productDTO.description());
        productInDB.setMetadata(productDTO.metadata());

        Product savedProduct = this.productRepository.save(productInDB);

        return this.productMapper.toDTO(savedProduct);
    }

    public ProductDTO updateByCode(String productCode, ProductDTO productDTO) {
        Product productInDB = this.productRepository.findByProductCode(productCode).orElseThrow(
                () -> new EntityNotFoundException("Product not found with id: " + productCode ));

        productInDB.setName(productDTO.name());
        productInDB.setPrice(productDTO.price());
        productInDB.setDescription(productDTO.description());
        productInDB.setMetadata(productDTO.metadata());

        Product savedProduct = this.productRepository.save(productInDB);

        return this.productMapper.toDTO(savedProduct);
    }

    public void delete(int id) {
        this.productRepository.deleteById(id);
    }

    public void deleteByCode(String productCode) {
        this.productRepository.deleteByProductCode(productCode);
    }
}
