package com.accshop.store.dto;

import java.time.LocalDateTime;
import java.util.Map;

public record ProductDTO(
        Long productId,
        String productCode,
        String name,
        String description,
        Double price,
        LocalDateTime createdAt,
        LocalDateTime updatedAt,
        Map<String, String> metadata
) {}
