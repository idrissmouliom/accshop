package com.accshop.store.repository;

import com.accshop.store.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    Optional<Product> findByProductCode(String productCode);

    void deleteByProductCode(String productCode);
}
