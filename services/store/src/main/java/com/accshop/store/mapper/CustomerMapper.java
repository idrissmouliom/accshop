package com.accshop.store.mapper;

import com.accshop.store.dto.CustomerDTO;
import com.accshop.store.entity.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

    public Customer toCustomer(CustomerDTO customerDTO) {
        if (customerDTO == null) {
            return null;
        }

        Customer customer = new Customer();
        customer.setCustomerId(customerDTO.customerId());
        customer.setCustomerCode(customerDTO.customerCode());
        customer.setFirstName(customerDTO.firstName());
        customer.setLastName(customerDTO.lastName());
        customer.setEmail(customerDTO.email());
        customer.setPassword(customerDTO.password());
        customer.setPhoneNumber(customerDTO.phoneNumber());
        customer.setCreatedAt(customerDTO.createdAt());
        customer.setUpdatedAt(customerDTO.updatedAt());
        customer.setMetadata(customerDTO.metadata());

        return customer;
    }

    public CustomerDTO toDTO(Customer customer) {
        if (customer == null) {
            return null;
        }

        return new CustomerDTO(
                customer.getCustomerId(),
                customer.getCustomerCode(),
                customer.getFirstName(),
                customer.getLastName(),
                customer.getEmail(),
                customer.getPassword(),
                customer.getPhoneNumber(),
                customer.getCreatedAt(),
                customer.getUpdatedAt(),
                customer.getMetadata()
        );
    }
}

