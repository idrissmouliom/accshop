package com.accshop.store.repository;


import com.accshop.store.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart,Integer> {
    Optional<Cart> findByCartCode(String cartCode);

    void deleteByCartCode(String cartCode);

}
