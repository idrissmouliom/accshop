package com.accshop.store.controller;

import com.accshop.store.dto.ProductDTO;
import com.accshop.store.entity.Product;
import com.accshop.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ProductDTO createProduct(@RequestBody ProductDTO productDTO){
        return this.productService.create(productDTO);
    }

    @GetMapping(path="{id}", produces = APPLICATION_JSON_VALUE)
    public Product getProduct(@PathVariable int id){
        return productService.get(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path="/code/{productCode}", produces = APPLICATION_JSON_VALUE)
    public ProductDTO getProductByCode(@PathVariable String productCode){
        return this.productService.getProductByCode(productCode);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path="/{id}", consumes = APPLICATION_JSON_VALUE)
    public ProductDTO update(@PathVariable int id, @RequestBody ProductDTO productDTO){
        return this.productService.update(id, productDTO);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path="/code/{productCode}", consumes = APPLICATION_JSON_VALUE)
    public ProductDTO updateByCode(@PathVariable String productCode, @RequestBody ProductDTO productDTO){
        return this.productService.updateByCode(productCode, productDTO);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path="/{id}")
    public void delete(@PathVariable int id){ this.productService.delete(id);}

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path="/code/{productCode}")
    public void deleteByCode(@PathVariable String productCode){ this.productService.deleteByCode(productCode);}


}
