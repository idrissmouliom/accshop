package com.accshop.store.dto;

import com.accshop.store.enums.PaymentStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

public record PaymentDTO(
        Long paymentId,
        String paymentCode,
        String purchaseCode,
        String customerCode,
        PaymentStatus paymentStatus,
        BigDecimal amount,
        String transactionId,
        LocalDateTime createdAt,
        LocalDateTime updatedAt,
        Map<String, String> metadata
) {}
