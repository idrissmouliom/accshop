package com.accshop.store.mapper;

import com.accshop.store.dto.AddressDTO;
import com.accshop.store.entity.Address;
import com.accshop.store.entity.Customer;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    public AddressDTO toDTO(Address address){

        return new AddressDTO(
                address.getAddressId(),
                address.getAddressCode(),
                address.getAddressType(),
                address.getStreet(),
                address.getCity(),
                address.getState(),
                address.getCountry(),
                address.getZipCode(),
                address.getCreatedAt(),
                address.getUpdatedAt(),
                address.getCustomer().getCustomerCode()
        );

    }

    public Address toAddress(AddressDTO addressDTO, Customer customer){
        Address address = new Address();
        address.setAddressId(addressDTO.addressId());
        address.setAddressCode(addressDTO.addressCode());
        address.setAddressType(addressDTO.addressType());
        address.setStreet(addressDTO.street());
        address.setCity(addressDTO.city());
        address.setState(addressDTO.state());
        address.setCountry(addressDTO.country());
        address.setZipCode(addressDTO.zipCode());
        address.setCreatedAt(addressDTO.createdAt());
        address.setUpdatedAt(addressDTO.updatedAt());
        address.setCustomer(customer);
        return address;
    }
}
