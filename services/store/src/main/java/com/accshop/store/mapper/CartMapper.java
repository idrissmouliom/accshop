package com.accshop.store.mapper;

import com.accshop.store.dto.CartDTO;
import com.accshop.store.dto.CartItemDTO;
import com.accshop.store.entity.Cart;
import com.accshop.store.entity.CartItem;
import com.accshop.store.entity.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CartMapper {

    private final CartItemMapper cartItemMapper;


    public CartMapper(CartItemMapper cartItemMapper) {
        this.cartItemMapper = cartItemMapper;
    }

    public CartDTO toDTO(Cart cart) {
        if (cart == null) {
            return null;
        }

        List<CartItemDTO> cartItemDTOs = cart.getCartItems() != null ?
                cart.getCartItems().stream()
                        .map(cartItemMapper::toDTO)
                        .collect(Collectors.toList()) :
                new ArrayList<>();

        return new CartDTO(
                cart.getCartId(),
                cart.getCartCode(),
                cartItemDTOs,
                cart.getCartItemsCount(),
                cart.getTotalPrice(),
                cart.getCurrency(),
                cart.getShippingMethod(),
                cart.getShippingCost(),
                cart.getCreatedAt(),
                cart.getUpdatedAt(),
                cart.getExpiryDate()
        );
    }

    public Cart toCart(CartDTO cartDTO) {
        if (cartDTO == null) {
            return null;
        }

        Cart cart = new Cart();
        updateCartFromDTO(cartDTO, cart);
        return cart;
    }

    public void updateCartFromDTO(CartDTO cartDTO, Cart cart) {
        if (cartDTO == null || cart == null) {
            return;
        }

        cart.setCartId(cartDTO.cartId());
        cart.setCartCode(cartDTO.cartCode());
        cart.setCartItemsCount(cartDTO.cartItemsCount());
        cart.setTotalPrice(cartDTO.totalPrice());
        cart.setCurrency(cartDTO.currency());
        cart.setShippingMethod(cartDTO.shippingMethod());
        cart.setShippingCost(cartDTO.shippingCost());
        cart.setCreatedAt(cartDTO.createdAt());
        cart.setUpdatedAt(cartDTO.updatedAt());
        cart.setExpiryDate(cartDTO.expiryDate());

        // Update CartItems
        if (cartDTO.cartItems() != null) {
            List<CartItem> updatedCartItems = cartDTO.cartItems().stream()
                    .map(this.cartItemMapper::toCartItem)
                    .collect(Collectors.toList());
            cart.setCartItems(updatedCartItems);

            // Ensure the cart reference is set for each CartItem
            updatedCartItems.forEach(item -> item.setCart(cart));
        } else {
            cart.setCartItems(new ArrayList<>());
        }
    }


    public List<CartDTO> toDTOList(List<Cart> carts) {
        if (carts == null) {
            return new ArrayList<>();
        }
        return carts.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}