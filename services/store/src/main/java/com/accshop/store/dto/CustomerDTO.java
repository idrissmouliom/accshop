package com.accshop.store.dto;

import jakarta.persistence.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.Map;

public record CustomerDTO(
        Long customerId,
        String customerCode,
        String firstName,
        String lastName,
        String email,
        String password,
        String phoneNumber,
        LocalDateTime createdAt,
        LocalDateTime updatedAt,
        Map<String, String> metadata
) {}
