package com.accshop.store.service;

import com.accshop.store.dto.CustomerCreateDTO;
import com.accshop.store.dto.CustomerDTO;
import com.accshop.store.dto.CustomerUpdateDTO;
import com.accshop.store.entity.Customer;
import com.accshop.store.mapper.CustomerCreateDTOMapper;
import com.accshop.store.mapper.CustomerMapper;
import com.accshop.store.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class CustomerService {

    private final CustomerCreateDTOMapper customerCreateDTOMapper;
    private final CustomerMapper customerMapper;
    private final CustomerRepository customerRepository;
    private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);

    public CustomerService(CustomerCreateDTOMapper customerCreateDTOMapper, CustomerMapper customerMapper, CustomerRepository customerRepository) {
        this.customerCreateDTOMapper = customerCreateDTOMapper;
        this.customerMapper = customerMapper;
        this.customerRepository = customerRepository;
    }

    public Customer create(CustomerCreateDTO customerCreateDTO){
        //log in the CustomerCreateDTO
        logger.debug("Received CustomerCreateDTO: {}",customerCreateDTO);

        Customer customer = customerCreateDTOMapper.apply(customerCreateDTO);

        //log in the CustomerCreateDTO
        logger.debug("Mapped Customer Entity: {}",customer);

        Customer savedCustomer = this.customerRepository.save(customer);

        //Log in Saved Customer
        logger.debug("Saved Customer: {}",savedCustomer);

        return savedCustomer;


    }

    public CustomerDTO get(int id) {
        Optional<Customer> optionalCustomer = this.customerRepository.findById(id);
        return optionalCustomer.map(customerMapper::toDTO).orElse(null);
    }

    public void update(int id, CustomerUpdateDTO customerUpdateDTO) {
        Optional<Customer> optionalCustomerInDB = this.customerRepository.findById(id);
        if(optionalCustomerInDB.isPresent()){
            Customer customerInDB = optionalCustomerInDB.get();
            if(Objects.equals(customerInDB.getEmail(), customerUpdateDTO.email())){
                customerInDB.setFirstName(customerUpdateDTO.firstName());
                customerInDB.setLastName(customerUpdateDTO.lastName());
                customerInDB.setPhoneNumber(customerUpdateDTO.phoneNumber());
                customerInDB.setMetadata(customerUpdateDTO.metadata());
                Customer updatedCustomer = this.customerRepository.save(customerInDB);
            }
        }
    }

    public void delete(int id) {
            this.customerRepository.deleteById(id);
    }

    public CustomerDTO getByCode(String customerCode) {
        Optional<Customer> optionalCustomer = this.customerRepository.findByCustomerCode(customerCode);
        return optionalCustomer.map(customerMapper::toDTO).orElse(null);
    }
}
