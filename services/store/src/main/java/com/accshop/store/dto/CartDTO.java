package com.accshop.store.dto;

import com.accshop.store.enums.Currency;
import com.accshop.store.enums.ShippingMethod;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public record CartDTO(
        Long cartId,
        String cartCode,
        List<CartItemDTO> cartItems,
        int cartItemsCount,
        BigDecimal totalPrice,
        Currency currency,
        ShippingMethod shippingMethod,
        BigDecimal shippingCost,
        LocalDateTime createdAt,
        LocalDateTime updatedAt,
        LocalDateTime expiryDate
) {}