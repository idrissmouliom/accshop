package com.accshop.store.mapper;

import com.accshop.store.dto.CustomerCreateDTO;
import com.accshop.store.entity.Customer;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
    public class CustomerCreateDTOMapper implements Function<CustomerCreateDTO, Customer> {

    @Override
    public Customer apply(CustomerCreateDTO customerCreateDTO) {
        if(customerCreateDTO == null){
            throw new NullPointerException("The customerCreateDTO should not be null.");
        }
        Customer customer = new Customer();
        customer.setFirstName(customerCreateDTO.firstName());
        customer.setLastName(customerCreateDTO.lastName());
        customer.setEmail(customerCreateDTO.email());
        customer.setPassword(customerCreateDTO.password());
        customer.setPhoneNumber(customerCreateDTO.phoneNumber());
        customer.setMetadata(customerCreateDTO.metadata());
        return customer;
    }
}
