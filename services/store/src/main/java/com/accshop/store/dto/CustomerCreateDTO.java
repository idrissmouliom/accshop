package com.accshop.store.dto;

import java.util.Map;

public record CustomerCreateDTO(
        String firstName,
        String lastName,
        String email,
        String password,
        String phoneNumber,
        Map<String,String> metadata
) {
}
