package com.accshop.store.repository;

import com.accshop.store.entity.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartItemRepository extends JpaRepository<CartItem,Integer> {
    Optional<CartItem> findByCartItemCode(String cartItemCode);
    void deleteByCartItemCode(String cartItemCode);
}
