package com.accshop.store.mapper;

import com.accshop.store.dto.PurchaseDTO;
import com.accshop.store.entity.Address;
import com.accshop.store.entity.Cart;
import com.accshop.store.entity.Customer;
import com.accshop.store.entity.Purchase;
import org.springframework.stereotype.Component;

@Component
public class PurchaseMapper {
    public PurchaseDTO toDTO(Purchase purchase) {
        if (purchase == null) {
            return null;
        }

        return new PurchaseDTO(
                purchase.getPurchaseId(),
                purchase.getPurchaseCode(),
                purchase.getCustomer().getCustomerCode(),
                purchase.getBillingAddress() != null ? purchase.getBillingAddress().getAddressCode() : null,
                purchase.getShippingAddress() != null ? purchase.getShippingAddress().getAddressCode() : null,
                purchase.getCart().getCartCode(),
                purchase.getCreatedAt(),
                purchase.getUpdatedAt(),
                purchase.getPurchaseStatus(),
                purchase.getPaymentStatus(),
                purchase.getMetadata()
        );
    }

    public Purchase toPurchase(PurchaseDTO purchaseDTO, Customer customer, Address billingAddress, Address shippingAddress, Cart cart) {
        if (purchaseDTO == null) {
            return null;
        }

        Purchase purchase = new Purchase();
        purchase.setPurchaseId(purchaseDTO.purchaseId());
        purchase.setPurchaseCode(purchaseDTO.purchaseCode());
        purchase.setCustomer(customer);
        purchase.setBillingAddress(billingAddress);
        purchase.setShippingAddress(shippingAddress);
        purchase.setCart(cart);
        purchase.setCreatedAt(purchaseDTO.createdAt());
        purchase.setUpdatedAt(purchaseDTO.updatedAt());
        purchase.setPurchaseStatus(purchaseDTO.purchaseStatus());
        purchase.setPaymentStatus(purchaseDTO.paymentStatus());
        purchase.setMetadata(purchaseDTO.metadata());

        return purchase;
    }
}
