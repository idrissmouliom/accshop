package com.accshop.store.service;

import com.accshop.store.dto.AddressDTO;
import com.accshop.store.dto.CustomerDTO;
import com.accshop.store.entity.Address;
import com.accshop.store.entity.Customer;
import com.accshop.store.mapper.AddressMapper;
import com.accshop.store.mapper.CustomerMapper;
import com.accshop.store.repository.AddressRepository;
import com.accshop.store.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService {


    private final AddressRepository addressRepository;
    private final CustomerService customerService;
    private final AddressMapper addressMapper;
    private final CustomerMapper customerMapper;

    public AddressService(AddressRepository addressRepository, CustomerService customerService, AddressMapper addressMapper, CustomerMapper customerMapper) {
        this.addressRepository = addressRepository;
        this.customerService = customerService;
        this.addressMapper = addressMapper;
        this.customerMapper = customerMapper;
    }

    public AddressDTO getAddress(int id) {
        Optional<Address> optionalAddress = this.addressRepository.findById(id);
        return optionalAddress.map(this.addressMapper::toDTO).orElse(null);
    }

    public AddressDTO getAddressByCode(String addressCode){
        Optional<Address> optionalAddress = this.addressRepository.findByAddressCode(addressCode);
        return optionalAddress.map(this.addressMapper::toDTO).orElse(null);
    }

    public AddressDTO create(AddressDTO addressDTO) {
        if(addressDTO.customerCode() == null){
            return null;
        }

        CustomerDTO customerDTO = this.customerService.getByCode(addressDTO.customerCode());
        Customer customer = this.customerMapper.toCustomer(customerDTO);

        Address newAddress = this.addressMapper.toAddress(addressDTO, customer);
        Address savedAddress = this.addressRepository.save(newAddress);

        return this.addressMapper.toDTO(savedAddress);
    }

    public AddressDTO updateByCode(String addressCode,AddressDTO addressDTO) {
        //verify address exist in DB
        Optional<Address> optionalAddress = this.addressRepository.findByAddressCode(addressCode);
        Address addressInDB = optionalAddress.orElse(null);

        //Modify AddressType - Street - City - State - Country - ZipCode
        assert addressInDB != null;
        addressInDB.setAddressType(addressDTO.addressType());
        addressInDB.setStreet(addressDTO.street());
        addressInDB.setCity(addressDTO.city());
        addressInDB.setState(addressDTO.state());
        addressInDB.setCountry(addressDTO.country());
        addressInDB.setZipCode(addressDTO.zipCode());

        //Save Address
        Address savedAddress = this.addressRepository.save(addressInDB);
        return this.addressMapper.toDTO(savedAddress);
    }

    public AddressDTO update(int id,AddressDTO addressDTO) {
        //verify address exist in DB
        Optional<Address> optionalAddress = this.addressRepository.findById(id);
        Address addressInDB = optionalAddress.orElse(null);

        //Modify AddressType - Street - City - State - Country - ZipCode
        addressInDB.setAddressType(addressDTO.addressType());
        addressInDB.setStreet(addressDTO.street());
        addressInDB.setCity(addressDTO.city());
        addressInDB.setState(addressDTO.state());
        addressInDB.setCountry(addressDTO.country());
        addressInDB.setZipCode(addressDTO.zipCode());

        //Save Address
        Address savedAddress = this.addressRepository.save(addressInDB);
        return this.addressMapper.toDTO(savedAddress);
    }

    public void delete(int id) {
        this.addressRepository.deleteById(id);
    }


    public void deleteByCode(String addressCode) {
        this.addressRepository.deleteByAddressCode(addressCode);
    }
}
