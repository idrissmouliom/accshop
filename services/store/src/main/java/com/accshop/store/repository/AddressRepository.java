package com.accshop.store.repository;

import com.accshop.store.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address,Integer> {
    Optional<Address> findByAddressCode(String AddressCode);

    void deleteByAddressCode(String addressCode);
}
