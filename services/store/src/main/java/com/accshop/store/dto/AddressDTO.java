package com.accshop.store.dto;

import com.accshop.store.enums.AddressType;

import java.time.LocalDateTime;

public record AddressDTO(Long addressId,
                         String addressCode,
                         AddressType addressType,
                         String street,
                         String city,
                         String state,
                         String country,
                         String zipCode,
                         LocalDateTime createdAt,
                         LocalDateTime updatedAt,
                         String customerCode
                         ) {
}
