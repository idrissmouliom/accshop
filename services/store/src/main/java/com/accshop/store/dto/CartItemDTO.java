package com.accshop.store.dto;


public record CartItemDTO(
        Long cartItemId,
        String cartItemCode,
        String cartCode,
        String productCode,
        int quantity,
        Double price
) {
}
