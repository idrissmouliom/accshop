package com.accshop.store.dto;

import java.util.Map;

public record CustomerUpdateDTO(String firstName,
                                String lastName,
                                String email,
                                String phoneNumber,
                                Map<String,String> metadata) {
}
