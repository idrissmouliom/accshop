package com.accshop.store.enums;

public enum ShippingMethod {
    FREE,
    STANDARD,
    PREMIUM
}
