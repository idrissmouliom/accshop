package com.accshop.store.enums;

public enum AddressType {
    SHIPPING,
    BILLING
}
