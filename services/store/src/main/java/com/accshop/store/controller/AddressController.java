package com.accshop.store.controller;

import com.accshop.store.dto.AddressDTO;
import com.accshop.store.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/addresses")
public class AddressController {

    @Autowired
    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public AddressDTO createAddress(@RequestBody AddressDTO addressDTO){
        return addressService.create(addressDTO);
    }

    @GetMapping(path = "{id}",produces = APPLICATION_JSON_VALUE)
    public AddressDTO getAddress(@PathVariable int id){
        return addressService.getAddress(id);
    }

    @GetMapping(path = "/code/{addressCode}", produces = APPLICATION_JSON_VALUE )
    public AddressDTO getAddressByCode(@PathVariable String addressCode){
        return addressService.getAddressByCode(addressCode);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PutMapping(path="/{id}")
    public AddressDTO updateAddress(@PathVariable int id,@RequestBody AddressDTO addressDTO){
        return addressService.update(id, addressDTO);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PutMapping(path="/code/{addressCode}")
    public AddressDTO updateAddressByCode(@PathVariable String addressCode,@RequestBody AddressDTO addressDTO){
        return addressService.updateByCode(addressCode, addressDTO);
    }

    @DeleteMapping(path="/{id}")
    public void delete(@PathVariable int id){ this.addressService.delete(id); }

    @DeleteMapping(path="/code/{addressCode}")
    public void deleteByCode(@PathVariable String addressCode) { this.addressService.deleteByCode(addressCode);}
}
