package com.accshop.store.mapper;

import com.accshop.store.dto.CartItemDTO;
import com.accshop.store.entity.Cart;
import com.accshop.store.entity.CartItem;
import com.accshop.store.entity.Product;
import com.accshop.store.repository.CartRepository;
import com.accshop.store.repository.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CartItemMapper {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;


    public CartItemMapper(CartRepository cartRepository, ProductRepository productRepository) {
        this.cartRepository = cartRepository;
        this.productRepository = productRepository;
    }

    public CartItemDTO toDTO(CartItem cartItem) {
        if (cartItem == null) {
            return null;
        }
        return new CartItemDTO(
                cartItem.getCartItemId(),
                cartItem.getCartItemCode(),
                cartItem.getCart().getCartCode(), // Extracting the cartCode from the Cart entity
                cartItem.getProduct().getProductCode(), // Extracting the productCode from the Product entity
                cartItem.getQuantity(),
                cartItem.getPrice()

        );
    }

    public CartItem toCartItem(CartItemDTO cartItemDTO) {
        if (cartItemDTO == null) {
            return null;
        }

        // Fetch Cart using cartCode from the DTO
        Optional<Cart> optionalCart = cartRepository.findByCartCode(cartItemDTO.cartCode());
        Cart cart = optionalCart.orElseThrow(() ->
                new EntityNotFoundException("Cart not found with code: " + cartItemDTO.cartCode()));

        // Fetch Product using productCode from the DTO
        Optional<Product> optionalProduct = productRepository.findByProductCode(cartItemDTO.productCode());
        Product product = optionalProduct.orElseThrow(() ->
                new EntityNotFoundException("Product not found with code: " + cartItemDTO.productCode()));

        CartItem cartItem = new CartItem();
        cartItem.setCartItemId(cartItemDTO.cartItemId());
        cartItem.setCartItemCode(cartItemDTO.cartItemCode());
        cartItem.setCart(cart);
        cartItem.setProduct(product);
        cartItem.setQuantity(cartItemDTO.quantity());
        cartItem.setPrice(cartItemDTO.price());

        return cartItem;
    }
}
