package com.accshop.store.enums;

public enum PaymentStatus {
    PENDING,
    COMPLETED,
    FAILED
}
