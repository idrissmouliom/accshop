package com.accshop.store.controller;

import com.accshop.store.dto.CartDTO;
import com.accshop.store.dto.CartItemDTO;
import com.accshop.store.entity.Cart;
import com.accshop.store.service.CartService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path="/api/carts")
public class CartController {

    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE )
    public void createCart(@RequestBody Cart cart){
        this.cartService.create(cart);
    }

    @GetMapping(path = "{id}", produces = APPLICATION_JSON_VALUE)
    public CartDTO getCart(@PathVariable int id){
        return this.cartService.get(id);
    }

    @GetMapping(path = "/code/{cartCode}", produces = APPLICATION_JSON_VALUE)
    public CartDTO getCartByCode(@PathVariable String cartCode){
        return this.cartService.getByCode(cartCode);
    }

    @PutMapping(path="/{id}", consumes = APPLICATION_JSON_VALUE)
    public CartDTO updateCart(@PathVariable int id,@RequestBody CartDTO cartDTO){ return this.cartService.update(id,cartDTO);}

    @PutMapping(path="/code/{cartCode}", consumes = APPLICATION_JSON_VALUE)
    public CartDTO updateCartByCode(@PathVariable String cartCode,@RequestBody CartDTO cartDTO){ return this.cartService.updateByCode(cartCode,cartDTO);}

    @DeleteMapping(path="/{id}")
    public void delete(@PathVariable int id){ this.cartService.delete(id);}

    @DeleteMapping(path="/code/{cartCode}")
    public void delete(@PathVariable String cartCode){ this.cartService.deleteByCode(cartCode);}

    //CartItem domain
    @PostMapping(path="/code/{cartCode}/items")
    public CartDTO addCartItemByCode(@PathVariable String cartCode, @RequestBody CartItemDTO cartItemDTO){
        return this.cartService.addCartItemByCode(cartCode, cartItemDTO);
    }

    @PutMapping(path="/code/{cartCode}/items/{cartItemCode}")
    public CartDTO updateCartItemQuantityByCode(@PathVariable String cartCode,@PathVariable String cartItemCode,@RequestParam int quantity){
       return this.cartService.updateCartItemQuantityByCode(cartCode,cartItemCode,quantity);
    }

    @DeleteMapping(path="/code/{cartCode}/items/{cartItemCode}")
    public void deleteCartItemByCode(@PathVariable String cartCode, @PathVariable String cartItemCode){
        this.cartService.deleteCartItemByCode(cartCode,cartItemCode);
    }

//    @GetMapping(path="/code/{cartCode}/items/{cartItemCode}")
//    public CartItemDTO getCartItemByCode(@PathVariable String cartCode, @PathVariable String cartItemCode){
//        return this.cartService.getCartItemByCode(cartCode,cartItemCode);
//    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(path="/items",consumes = APPLICATION_JSON_VALUE)
    public CartItemDTO createCartItem(@RequestBody CartItemDTO cartItemDTO){
        return this.cartService.createCartItem(cartItemDTO);
    }

    @GetMapping(path="/items/code/{cartItemCode}")
    public CartItemDTO getCartItem(@PathVariable String cartItemCode){
        return this.cartService.getCartItemByCode(cartItemCode);
    }

    @GetMapping(path="/items/{id}")
    public CartItemDTO getCartItem(@PathVariable int id){
        return this.cartService.getCartItem(id);
    }

    @DeleteMapping(path="/items/code/{cartItemCode}")
    public void deleteCartItemByCode(@PathVariable String cartItemCode){
        this.cartService.deleteCartItemByCodeAbsolute(cartItemCode);
    }
}
