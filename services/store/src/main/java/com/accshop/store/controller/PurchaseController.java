package com.accshop.store.controller;

import com.accshop.store.dto.PaymentDTO;
import com.accshop.store.dto.PurchaseDTO;
import com.accshop.store.entity.Purchase;
import com.accshop.store.service.PurchaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/purchases")
public class PurchaseController {

    private PurchaseService purchaseService;

    public PurchaseController(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }

    @GetMapping(path="/{id}", produces = APPLICATION_JSON_VALUE)
    public PurchaseDTO getPurchase(@PathVariable int id){
        return this.purchaseService.get(id);
    }

    @GetMapping(path="/code/{purchaseCode}", produces = APPLICATION_JSON_VALUE)
    public PurchaseDTO getPurchaseByCode(@PathVariable String purchaseCode){ return this.purchaseService.getPurchaseByCode(purchaseCode);}

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public PurchaseDTO createPurchase(@RequestBody PurchaseDTO purchaseDTO){
        return this.purchaseService.create(purchaseDTO);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public PurchaseDTO updatePurchase(@PathVariable int id,@RequestBody PurchaseDTO purchaseDTO){
        return this.purchaseService.update(id,purchaseDTO);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/code/{purchaseCode}", consumes = APPLICATION_JSON_VALUE)
    public PurchaseDTO updatePurchase(@PathVariable String purchaseCode,@RequestBody PurchaseDTO purchaseDTO){
        return this.purchaseService.updateByCode(purchaseCode,purchaseDTO);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable int id){ this.purchaseService.delete(id);}

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path="/code/{purchaseCode}")
    public void deleteByCode(@PathVariable String purchaseCode){
        this.purchaseService.deleteByCode(purchaseCode);
    }

    //Payment Methods
//    @PostMapping(path = "/payment/{purchaseCode}")
//    public CompletableFuture<ResponseEntity<PaymentDTO>> createPaymentForPurchase(@PathVariable String purchaseCode, @RequestBody PaymentDTO paymentDTO){
//        return purchaseService.createPaymentAsync(paymentDTO).thenApply(createdPayment -> ResponseEntity.status(HttpStatus.CREATED).body(createdPayment));
//    }
//
//    @GetMapping(path = "/payment/{purchaseCode}", produces = APPLICATION_JSON_VALUE)
//    public CompletableFuture<ResponseEntity<PaymentDTO>> getPaymentByPurchaseCode(@PathVariable String purchaseCode){
//        return purchaseService.getPaymentByPurchaseCodeAsync(purchaseCode).thenApply(payment -> payment != null ? ResponseEntity.ok(payment) : ResponseEntity.notFound().build());
//    }
}
