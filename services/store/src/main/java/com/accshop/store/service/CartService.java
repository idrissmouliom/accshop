package com.accshop.store.service;

import com.accshop.store.dto.CartDTO;
import com.accshop.store.dto.CartItemDTO;
import com.accshop.store.entity.Cart;
import com.accshop.store.entity.CartItem;
import com.accshop.store.entity.Product;
import com.accshop.store.mapper.CartItemMapper;
import com.accshop.store.mapper.CartMapper;
import com.accshop.store.repository.CartItemRepository;
import com.accshop.store.repository.CartRepository;
import com.accshop.store.repository.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class CartService {

    private final CartRepository cartRepository;
    private final CartMapper cartMapper;
    private final CartItemMapper cartItemMapper;
    private final CartItemRepository cartItemRepository;

    public CartService(CartRepository cartRepository, CartMapper cartMapper, CartItemMapper cartItemMapper, CartItemRepository cartItemRepository) {
        this.cartRepository = cartRepository;
        this.cartMapper = cartMapper;
        this.cartItemMapper = cartItemMapper;
        this.cartItemRepository = cartItemRepository;
    }

    public CartDTO get(int id) {
        Cart cartInDB = this.cartRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Cart not found with id: " + id));
        return this.cartMapper.toDTO(cartInDB);
    }

    public CartDTO getByCode(String cartCode) {
        Cart cartInDB = this.cartRepository.findByCartCode(cartCode).orElseThrow(() -> new EntityNotFoundException("Cart not found with code: " +  cartCode));
        return this.cartMapper.toDTO(cartInDB);
    }

    public void create(Cart cart) {
        this.cartRepository.save(cart);
    }

    public CartDTO update(int id, CartDTO cartDTO) {
        Cart cartInDB = this.cartRepository.findById(id).orElseThrow(
                ()-> new EntityNotFoundException("Cart not found with id" +id));

        cartInDB.setCurrency(cartDTO.currency());
        cartInDB.setShippingCost(cartDTO.shippingCost());
        cartInDB.setShippingMethod(cartDTO.shippingMethod());

        Cart savedInDB = this.cartRepository.save(cartInDB);

        return this.cartMapper.toDTO(savedInDB);
    }

    public CartDTO updateByCode(String cartCode, CartDTO cartDTO) {
        Cart cartInDB = this.cartRepository.findByCartCode(cartCode).orElseThrow(
                ()-> new EntityNotFoundException("Cart not found with Cart Code" + cartCode));

        cartInDB.setCurrency(cartDTO.currency());
        cartInDB.setShippingCost(cartDTO.shippingCost());
        cartInDB.setShippingMethod(cartDTO.shippingMethod());

        Cart savedInDB = this.cartRepository.save(cartInDB);

        return this.cartMapper.toDTO(savedInDB);
    }


    public void delete(int id) {
        this.cartRepository.deleteById(id);
    }

    public void deleteByCode(String cartCode){
        this.cartRepository.deleteByCartCode(cartCode);
    }

    public CartDTO addCartItemByCode(String cartCode,CartItemDTO cartItemDTO){
        //retrieve/verify cart exists
        Cart cartInDB = this.cartRepository.findByCartCode(cartCode).orElseThrow(()-> new EntityNotFoundException("Cart not found with code:" + cartCode));

        //Create CartItem Entity
        CartItem newCartItem = this.cartItemMapper.toCartItem(cartItemDTO);

        cartInDB.getCartItems().add(newCartItem);

        //Update Cart Count and Total price
        cartInDB.setCartItemsCount((int) cartInDB.getCartItems().stream().mapToDouble(CartItem::getQuantity).sum());
        cartInDB.setTotalPrice(BigDecimal.valueOf(cartInDB.getCartItems().stream().mapToDouble(item -> item.getPrice()*item.getQuantity()).sum()));

        //Save the Cart and the CartItem
        this.cartItemRepository.save(newCartItem);
        Cart savedCart = this.cartRepository.save(cartInDB);

        return this.cartMapper.toDTO(savedCart);

    }

    public CartDTO updateCartItemQuantityByCode(String cartCode, String cartItemCode, int quantity) {
        Cart cartInDB = this.cartRepository.findByCartCode(cartCode).orElseThrow(()->new EntityNotFoundException("Cart not found with code: " + cartCode));

        CartItem cartItem = cartInDB.getCartItems()
                                    .stream()
                                    .filter(item -> item.getCartItemCode().equals(cartItemCode))
                                    .findFirst()
                                    .orElseThrow(()-> new EntityNotFoundException("CartItem not found with code: " + cartItemCode));

        cartItem.setQuantity((int) quantity);

        cartInDB.setCartItemsCount((int) cartInDB.getCartItems().stream().mapToDouble(CartItem::getQuantity).sum());
        cartInDB.setTotalPrice(BigDecimal.valueOf(cartInDB.getCartItems()
                                        .stream()
                                        .mapToDouble(item-> item.getPrice()*item.getQuantity())
                                        .sum()));

        this.cartItemRepository.save(cartItem);
        this.cartRepository.save(cartInDB);

        return this.cartMapper.toDTO(cartInDB);
    }


    @Transactional
    public void deleteCartItemByCode(String cartCode, String cartItemCode) {
        Cart cartInDB = this.cartRepository.findByCartCode(cartCode).orElseThrow(()-> new EntityNotFoundException("Cart not found with code: "+ cartCode));

        CartItem cartItemtoRemove = cartInDB.getCartItems()
                                            .stream()
                                            .filter(item -> item.getCartItemCode()
                                            .equals(cartItemCode))
                                            .findFirst()
                                            .orElseThrow(()->new EntityNotFoundException("CartItem not found with code: "+ cartItemCode));
        cartInDB.getCartItems().remove(cartItemtoRemove);


        this.cartRepository.save(cartInDB);
        this.cartItemRepository.delete(cartItemtoRemove);
    }

//    public CartItemDTO getCartItemByCode(String cartCode, String cartItemCode) {
//        Cart cartInDB = this.cartRepository.findByCartCode(cartCode).orElseThrow(()->new EntityNotFoundException("Cart not found with code: " + cartCode));
//        CartItem cartItem = cartInDB.getCartItems()
//                .stream()
//                .filter(item -> item.getCartItemCode().equals(cartItemCode))
//                .findFirst()
//                .orElseThrow(()-> new EntityNotFoundException("CartItem not found with code: " + cartItemCode));
//
//        return this.cartItemMapper.toDTO(cartItem);
//    }

    public CartItemDTO getCartItemByCode(String cartItemCode) {
        CartItem cartItemInDB = this.cartItemRepository.findByCartItemCode(cartItemCode).orElseThrow(() -> new EntityNotFoundException("Cart not found wih code: " + cartItemCode));

        return this.cartItemMapper.toDTO(cartItemInDB);
    }

    public CartItemDTO getCartItem(int id) {
        CartItem cartItemInDB = this.cartItemRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("CartItem not found with id: " + id));
        return cartItemMapper.toDTO(cartItemInDB);
    }

    public void deleteCartItemByCodeAbsolute(String cartItemCode){
        this.cartItemRepository.deleteByCartItemCode(cartItemCode);
    }

    public CartItemDTO createCartItem(CartItemDTO cartItemDTO) {
        if (cartItemDTO.productCode() == null || cartItemDTO.cartCode() == null) {
            throw new IllegalArgumentException("Product code and cart code must not be null");
        }

        // Optionally, check for empty strings or other validation rules here
        if (cartItemDTO.productCode().isEmpty() || cartItemDTO.cartCode().isEmpty()) {
            throw new IllegalArgumentException("Product code and cart code must not be empty");
        }

        // Convert DTO to entity and save
        CartItem cartItem = this.cartItemMapper.toCartItem(cartItemDTO);
        CartItem savedCartItem = this.cartItemRepository.save(cartItem);

        // Convert saved entity back to DTO and return
        return this.cartItemMapper.toDTO(savedCartItem);
    }
}
