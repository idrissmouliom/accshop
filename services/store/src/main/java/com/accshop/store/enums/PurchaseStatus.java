package com.accshop.store.enums;

public enum PurchaseStatus {
    PENDING,
    SHIPPED,
    DELIVERED,
    CANCELED
}
