package com.accshop.store.dto;

import com.accshop.store.enums.PaymentStatus;
import com.accshop.store.enums.PurchaseStatus;

import java.time.LocalDateTime;
import java.util.Map;

public record PurchaseDTO(
        Long purchaseId,
        String purchaseCode,
        String customerCode,
        String billingAddressCode,
        String shippingAddressCode,
        String cartCode,
        LocalDateTime createdAt,
        LocalDateTime updatedAt,
        PurchaseStatus purchaseStatus,
        PaymentStatus paymentStatus,
        Map<String, String> metadata
) {}
