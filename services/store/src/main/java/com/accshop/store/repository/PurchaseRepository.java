package com.accshop.store.repository;

import com.accshop.store.entity.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {
    Optional<Purchase> findByPurchaseCode(String purchaseCode);

    void deleteByPurchaseCode(String purchaseCode);
}
