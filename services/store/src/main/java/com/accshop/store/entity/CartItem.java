package com.accshop.store.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "cart_item")
public class CartItem {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cart_item_id_seq")
    @SequenceGenerator(name = "cart_item_id_seq", sequenceName = "cart_item_id_seq", allocationSize = 1)
    @Column(name = "cart_item_id")
    private Long cartItemId;

    @Column(name = "cart_item_code", unique = true, nullable = false, updatable = false, insertable = false)
    private String cartItemCode;

    @ManyToOne
    @JoinColumn(name = "cart_code",referencedColumnName = "cart_code")
    private Cart cart;

    @ManyToOne
    @JoinColumn(name = "product_code",referencedColumnName = "product_code")
    private Product product;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private Double price;

    public CartItem(Long cartItemId, String cartItemCode, Cart cart, Product product, int quantity, Double price) {
        this.cartItemId = cartItemId;
        this.cartItemCode = cartItemCode;
        this.cart = cart;
        this.product = product;
        this.quantity = quantity;
        this.price = price;
    }

    public CartItem() {

    }

    public String getCartItemCode() {
        return cartItemCode;
    }

    public Long getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(Long cartItemId) {
        this.cartItemId = cartItemId;
    }

    public void setCartItemCode(String cartItemCode) {
        this.cartItemCode = cartItemCode;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
