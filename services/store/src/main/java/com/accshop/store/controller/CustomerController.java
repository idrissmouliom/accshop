package com.accshop.store.controller;

import com.accshop.store.dto.CustomerCreateDTO;
import com.accshop.store.dto.CustomerDTO;
import com.accshop.store.dto.CustomerUpdateDTO;
import com.accshop.store.entity.Customer;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.accshop.store.service.CustomerService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/customers")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public Customer createCustomer(@RequestBody CustomerCreateDTO customerCreateDTO){
        return this.customerService.create(customerCreateDTO);
    }

    @GetMapping(path="{id}",produces = APPLICATION_JSON_VALUE)
    public CustomerDTO getCustomer(@PathVariable int id){
        return this.customerService.get(id);
    }

    @GetMapping(path="/code/{customerCode}")
    public CustomerDTO getCustomerByCode(@PathVariable String customerCode){
        return this.customerService.getByCode(customerCode);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PutMapping(path = "{id}",consumes = APPLICATION_JSON_VALUE)
    public void updateCustomer(@PathVariable int id,@RequestBody CustomerUpdateDTO customerUpdateDTO){
        this.customerService.update(id,customerUpdateDTO);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "{id}")
    public void deleteCustomer(@PathVariable int id){
        this.customerService.delete(id);
    }


}
