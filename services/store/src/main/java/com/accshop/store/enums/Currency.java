package com.accshop.store.enums;

public enum Currency {
        USD("United States Dollar"),
        EUR("Euro"),
        GBP("British Pound Sterling"),
        JPY("Japanese Yen"),
        CAD("Canadian Dollar"),
        AUD("Australian Dollar"),
        CHF("Swiss Franc"),
        CNY("Chinese Yuan"),
        SEK("Swedish Krona"),
        NZD("New Zealand Dollar");

        private final String description;

        Currency(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
}

