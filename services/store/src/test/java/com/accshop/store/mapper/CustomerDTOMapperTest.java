package com.accshop.store.mapper;

import com.accshop.store.dto.CustomerDTO;
import com.accshop.store.entity.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CustomerDTOMapperTest {

    private CustomerMapper customerMapper;

    @BeforeEach
    void setUp() {
        customerMapper = new CustomerMapper();
    }

    @Test
    public void shouldMapCustomerToCustomerDTO(){
        //Given(Input)
        Map<String,String> preferences = new HashMap<>();
        preferences.put("city","Los Angeles");
        preferences.put("sport","Sky diving");

        Customer customer = new Customer(
                1L,
                "CUST-0001",
                "Thomas",
                "Anderson",
                "thomas.anderson@metacortex.com",
                "1-000-000-001",
                preferences
        );

        //When
        CustomerDTO customerDTO = customerMapper.toDTO(customer);

        //Then
        Assertions.assertEquals(customer.getCustomerId(),customerDTO.customerId());
        Assertions.assertEquals(customer.getCustomerCode(),customerDTO.customerCode());
        Assertions.assertEquals(customer.getFirstName(),customerDTO.firstName());
        Assertions.assertEquals(customer.getLastName(),customerDTO.lastName());
        Assertions.assertEquals(customer.getEmail(),customerDTO.email());
        Assertions.assertEquals(customer.getPhoneNumber(),customerDTO.phoneNumber());
        Assertions.assertEquals(customer.getMetadata(),customerDTO.metadata());
    }
}