package com.accshop.store.mapper;

import com.accshop.store.dto.CustomerCreateDTO;
import com.accshop.store.entity.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CustomerCreateDTOMapperTest {

        private CustomerCreateDTOMapper customerCreateDTOMapper;
    @BeforeEach
    void setUp() {
        customerCreateDTOMapper = new CustomerCreateDTOMapper();
    }

    @Test
    public void shouldMapCustomerCreateDTOToCustomer(){
        //Input
        Map<String,String> preferences = new HashMap<>();
        preferences.put("city","Los Angeles");
        preferences.put("sport","Skydiving");
        CustomerCreateDTO customerCreateDTO = new CustomerCreateDTO(
                "Thomas",
                "Anderson",
                "thomas.anderson@metacortex.com",
                "neotheone",
                "1-000-000-0001",
                preferences
        );

        //Action
        Customer customer= customerCreateDTOMapper.apply(customerCreateDTO);

        //Asserts
        Assertions.assertEquals(customer.getFirstName(),customerCreateDTO.firstName());
        Assertions.assertEquals(customer.getLastName(),customerCreateDTO.lastName());
        Assertions.assertEquals(customer.getEmail(),customerCreateDTO.email());
        Assertions.assertEquals(customer.getPassword(),customerCreateDTO.password());
        Assertions.assertEquals(customer.getPhoneNumber(),customerCreateDTO.phoneNumber());
        Assertions.assertEquals(customer.getMetadata(),customerCreateDTO.metadata());
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenCustomerCreateDTOIsNull(){
        //Actions x Asserts
        var exception = assertThrows(NullPointerException.class,()->customerCreateDTOMapper.apply(null));

        assertEquals(exception.getMessage(),"The customerCreateDTO should not be null.");
    }

}