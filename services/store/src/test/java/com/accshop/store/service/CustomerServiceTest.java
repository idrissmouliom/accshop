package com.accshop.store.service;

import com.accshop.store.dto.CustomerCreateDTO;
import com.accshop.store.dto.CustomerDTO;
import com.accshop.store.dto.CustomerUpdateDTO;
import com.accshop.store.entity.Customer;
import com.accshop.store.mapper.CustomerCreateDTOMapper;
import com.accshop.store.mapper.CustomerMapper;
import com.accshop.store.repository.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    //Service
    @InjectMocks
    private CustomerService customerService;

    //Dependencies
    @Mock
    private CustomerCreateDTOMapper customerCreateDTOMapper;

    @Mock
    private CustomerMapper customerMapper;

    @Mock
    private CustomerRepository customerRepository;


    @Test
    public void shouldCreateSuccessfullyCustomer(){
        //Input & Preparation
        Map<String,String> preferences = new HashMap<>();
        preferences.put("city","Los Angeles");
        preferences.put("sport","Skydiving");
        CustomerCreateDTO customerCreateDTO = new CustomerCreateDTO(
                "Thomas",
                "Anderson",
                "thomas.anderson@metacortex.com",
                "neotheone",
                "1-000-000-0001",
                preferences);

        Customer customer = new Customer();
        customer.setFirstName("Thomas");
        customer.setLastName("Anderson");
        customer.setEmail("thomas.anderson@metacortex.com");
        customer.setPassword("neotheone");
        customer.setPhoneNumber("1-000-000-0001");
        customer.setMetadata(preferences);

        Customer savedCustomer = new Customer();
        savedCustomer.setCustomerId(1L);
        savedCustomer.setCustomerCode("CUST-001");
        savedCustomer.setFirstName("Thomas");
        savedCustomer.setLastName("Anderson");
        savedCustomer.setEmail("thomas.anderson@metacortex.com");
        savedCustomer.setPassword("neotheone");
        savedCustomer.setPhoneNumber("1-000-000-0001");
        savedCustomer.setMetadata(preferences);

        //Mock of the calls
        Mockito.when(customerCreateDTOMapper.apply(customerCreateDTO))
                .thenReturn(customer);

        Mockito.when(customerRepository.save(customer))
                .thenReturn(savedCustomer);

        //Actions
        customerService.create(customerCreateDTO);

        //Assertions and Verifications
        Assertions.assertEquals(customerCreateDTO.firstName(),customer.getFirstName());
        Assertions.assertEquals(customerCreateDTO.lastName(),customer.getLastName());
        Assertions.assertEquals(customerCreateDTO.email(),customer.getEmail());
        Assertions.assertEquals(customerCreateDTO.password(),customer.getPassword());
        Assertions.assertEquals(customerCreateDTO.phoneNumber(),customer.getPhoneNumber());
        Assertions.assertEquals(customerCreateDTO.metadata(),customer.getMetadata());

        Assertions.assertEquals(savedCustomer.getFirstName(),customer.getFirstName());
        Assertions.assertEquals(savedCustomer.getLastName(),customer.getLastName());
        Assertions.assertEquals(savedCustomer.getEmail(),customer.getEmail());
        Assertions.assertEquals(savedCustomer.getPassword(),customer.getPassword());
        Assertions.assertEquals(savedCustomer.getPhoneNumber(),customer.getPhoneNumber());
        Assertions.assertEquals(savedCustomer.getMetadata(),customer.getMetadata());
        Assertions.assertNotNull(savedCustomer.getCustomerId());
        Assertions.assertNotNull(savedCustomer.getCustomerCode());

        verify(customerCreateDTOMapper, times(1)).apply(customerCreateDTO);
        verify(customerRepository, times(1)).save(customer);

    }

    @Test
    public void ShouldReturnCustomerDTOById(){
        //Input & Preparation
        Map<String,String> preferences = new HashMap<>();
        preferences.put("city","Los Angeles");
        preferences.put("sport","Skydiving");
        Customer customer = new Customer();
        customer.setCustomerId(1L);
        customer.setCustomerCode("CUST-001");
        customer.setFirstName("Thomas");
        customer.setLastName("Anderson");
        customer.setEmail("thomas.anderson@metacortex.com");
        customer.setPassword("neotheone");
        customer.setPhoneNumber("1-000-000-0001");
        customer.setMetadata(preferences);

        Optional<Customer> optionalCustomer = Optional.of(customer);

        CustomerDTO customerDTO = new CustomerDTO(
                customer.getCustomerId(),
                customer.getCustomerCode(),
                customer.getFirstName(),
                customer.getLastName(),
                customer.getEmail(),
                customer.getPassword(),
                customer.getPhoneNumber(),
                customer.getCreatedAt(),
                customer.getUpdatedAt(),
                customer.getMetadata()
        );

        //Mock the calls
        Mockito.when(customerRepository.findById(anyInt())).thenReturn(optionalCustomer);
        Mockito.when(customerMapper.toDTO(Mockito.any(Customer.class))).thenReturn(customerDTO);

;        //Actions
        CustomerDTO responseDTO = customerService.get(1);

        //Assertions
        Assertions.assertEquals(customerDTO.customerId(), responseDTO.customerId());
        Assertions.assertEquals(customerDTO.customerCode(), responseDTO.customerCode());
        Assertions.assertEquals(customerDTO.firstName(), responseDTO.firstName());
        Assertions.assertEquals(customerDTO.lastName(), responseDTO.lastName());
        Assertions.assertEquals(customerDTO.email(), responseDTO.email());
        Assertions.assertEquals(customerDTO.phoneNumber(), responseDTO.phoneNumber());
        Assertions.assertEquals(customerDTO.metadata(), responseDTO.metadata());

        // Verify that findById and apply methods were called once
        verify(customerRepository, Mockito.times(1)).findById(1);
        verify(customerMapper, Mockito.times(1)).toDTO(customer);

    }

    @Test
    public void ShouldUpdateSuccessfullyCustomer(){
        //Input & Preparation
        Map<String,String> updatedPreferences = new HashMap<>();
        updatedPreferences.put("city","Zion");
        updatedPreferences.put("sport","Kung Fu");
        CustomerUpdateDTO updatedCustomerDTO = new CustomerUpdateDTO(
                "Neo",
                "TheOne",
                   "thomas.anderson@metacortex.com",
                "1-000-000-0001",
                updatedPreferences);

        Map<String,String> preferences = new HashMap<>();
        preferences.put("city","Los Angeles");
        preferences.put("sport","SkyDiving");
        Customer databaseCustomer = new Customer();
        databaseCustomer.setCustomerId(1L);
        databaseCustomer.setCustomerCode("CUST-001");
        databaseCustomer.setFirstName("Thomas");
        databaseCustomer.setLastName("Anderson");
        databaseCustomer.setEmail("thomas.anderson@metacortex.com");
        databaseCustomer.setPassword("neotheone");
        databaseCustomer.setPhoneNumber("1-000-000-0001");
        databaseCustomer.setMetadata(preferences);

        Optional<Customer> optionalCustomer = Optional.of(databaseCustomer);

        Customer updatedCustomer = new Customer();
        updatedCustomer.setCustomerId(1L);
        updatedCustomer.setCustomerCode("CUST-001");
        updatedCustomer.setFirstName("Neo");
        updatedCustomer.setLastName("TheOne");
        updatedCustomer.setEmail("thomas.anderson@metacortex.com");
        updatedCustomer.setPassword("neotheone");
        updatedCustomer.setPhoneNumber("1-000-000-0001");
        updatedCustomer.setMetadata(updatedPreferences);

        //Mocking the calls
        Mockito.when(customerRepository.findById(anyInt())).thenReturn(optionalCustomer);
        Mockito.when(customerRepository.save(any(Customer.class))).thenReturn(updatedCustomer);

        //Actions

        customerService.update(1, updatedCustomerDTO);

        //Assertions
        assertEquals("Neo", updatedCustomer.getFirstName());
        assertEquals("TheOne", updatedCustomer.getLastName());
        assertEquals("1-000-000-0001", updatedCustomer.getPhoneNumber());
        assertEquals(updatedPreferences, updatedCustomer.getMetadata());

        verify(customerRepository, times(1)).findById(1);
        verify(customerRepository, times(1)).save(any(Customer.class));
    }
}