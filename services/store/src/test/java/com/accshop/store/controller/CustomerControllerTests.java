package com.accshop.store.controller;

import com.accshop.store.dto.CustomerCreateDTO;
import com.accshop.store.dto.CustomerDTO;
import com.accshop.store.dto.CustomerUpdateDTO;
import com.accshop.store.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.StatusAssertions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.util.HashMap;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(SpringExtension.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    @Test
    public void createCustomer_ShouldReturnStatusCreated() throws Exception{
        CustomerCreateDTO customerCreateDTO = new CustomerCreateDTO(
                "Thomas",
                "Anderson",
                "thomas.anderson@metacortex.com",
                "neotheone",
                "1-000-000-0001",
                new HashMap<>()
        );

        StatusAssertions status;
        mockMvc.perform(MockMvcRequestBuilders.post("/api/customers")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"firstName\": \"Thomas\", \"lastName\": \"Anderson\", \"email\": \"thomas.anderson@metacortex.com\", \"password\": \"neotheone\", \"phoneNumber\": \"1-000-000-0001\", \"metadata\": {} }"))
                .andExpect(status().isCreated());
    }

    @Test
    public void getCustomer_ShouldReturnCustomerDTO() throws Exception {
        // Prepare a mock CustomerDTO
        CustomerDTO customerDTO = new CustomerDTO(
                1L,
                "CUST-001",
                "Thomas",
                "Anderson",
                "thomas.anderson@metacortex.com",
                "neotheone", // You missed the password field in the original code
                "1-000-000-0001",
                LocalDateTime.now(),  // Make sure to provide createdAt value
                LocalDateTime.now(),  // Make sure to provide updatedAt value
                new HashMap<>()  // Make sure metadata is provided
        );

        // Mock the customerService.get method
        Mockito.when(customerService.get(ArgumentMatchers.anyInt()))
                .thenReturn(customerDTO);

        // Perform a GET request to the /api/customers/1 endpoint
        mockMvc.perform(MockMvcRequestBuilders.get("/api/customers/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customerId").value(1L))  // Use 1L for matching long value
                .andExpect(jsonPath("$.customerCode").value("CUST-001"))
                .andExpect(jsonPath("$.firstName").value("Thomas"))
                .andExpect(jsonPath("$.lastName").value("Anderson"))
                .andExpect(jsonPath("$.email").value("thomas.anderson@metacortex.com"))
                .andExpect(jsonPath("$.phoneNumber").value("1-000-000-0001"));
    }


    @Test
    public void updateCustomer_ShouldReturnStatusNoContent() throws Exception{
        CustomerUpdateDTO customerUpdateDTO = new CustomerUpdateDTO(
                "Neo",
                "The One",
                "thomas.anderson@metacortex.com",
                "1-000-000-0002",
                new HashMap<>()
        );

        mockMvc.perform(MockMvcRequestBuilders.put("/api/customers/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"firstName\": \"Neo\", \"lastName\": \"The One\", \"phoneNumber\": \"1-000-000-0002\", \"metadata\": {} }"))
                        .andExpect(status().isNoContent());
    }

    @Test
    public void deleteCustomer_ShouldReturnStatusNoContent() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/customers/1"))
                .andExpect(status().isNoContent());
    }
}
