package com.accshop.payment.enums;

public enum PaymentStatus {
    PENDING,
    COMPLETED,
    FAILED
}
