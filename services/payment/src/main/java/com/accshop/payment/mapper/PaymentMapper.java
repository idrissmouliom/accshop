package com.accshop.payment.mapper;

import com.accshop.payment.dto.PaymentDTO;
import com.accshop.payment.entity.Payment;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class PaymentMapper {

    private static final Logger logger = LoggerFactory.getLogger(PaymentMapper.class);

    public PaymentDTO toDTO(Payment payment) {
        if (payment == null) {
            return null;
        }
        return new PaymentDTO(
                payment.getPaymentId(),
                payment.getPaymentCode(),
                payment.getPurchaseCode(),
                payment.getCustomerCode(),
                payment.getPaymentStatus(),
                payment.getAmount(),
                payment.getCreatedAt(),
                payment.getUpdatedAt(),
                payment.getMetadata()
        );
    }

    public Payment toPayment(PaymentDTO paymentDTO) {
        if (paymentDTO == null) {
            return null;
        }
        Payment payment = new Payment();
        payment.setPaymentId(paymentDTO.paymentId());
        payment.setPurchaseCode(paymentDTO.purchaseCode());
        payment.setCustomerCode(paymentDTO.customerCode());
        payment.setPaymentStatus(paymentDTO.paymentStatus());
        payment.setAmount(paymentDTO.amount());
        payment.setCreatedAt(paymentDTO.createdAt());
        payment.setUpdatedAt(paymentDTO.updatedAt());
        payment.setMetadata(paymentDTO.metadata());
        return payment;
    }
}
