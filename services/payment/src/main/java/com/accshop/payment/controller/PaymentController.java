package com.accshop.payment.controller;

import com.accshop.payment.dto.PaymentDTO;
import com.accshop.payment.entity.Payment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.accshop.payment.service.PaymentService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/payments")
public class PaymentController {

    private PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public PaymentDTO getPayment(@PathVariable int id){
        return this.paymentService.get(id);
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public PaymentDTO createPayment(@RequestBody PaymentDTO paymentDTO){
        return this.paymentService.create(paymentDTO);
    }

    @GetMapping(path = "/code/{paymentCode}")
    public ResponseEntity<PaymentDTO> getPaymentByCode(@PathVariable String paymentCode){
        PaymentDTO paymentDTO = this.paymentService.getPaymentByCode(paymentCode);
        return paymentDTO != null ? ResponseEntity.ok(paymentDTO) : ResponseEntity.notFound().build();
    }

    @GetMapping(path = "/code/{purchaseCode}")
    public ResponseEntity<PaymentDTO> getPaymentByPurchaseCode(@PathVariable String purchaseCode){
        PaymentDTO paymentDTO = this.paymentService.getPaymentByPurchaseCode(purchaseCode);
        return paymentDTO != null ? ResponseEntity.ok(paymentDTO) : ResponseEntity.notFound().build();
    }
}
