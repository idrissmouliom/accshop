package com.accshop.payment.service;

import com.accshop.payment.dto.PaymentDTO;
import com.accshop.payment.entity.Payment;
import com.accshop.payment.mapper.PaymentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.accshop.payment.repository.PaymentRepository;

import java.util.Optional;


@Service
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private PaymentMapper paymentMapper;
    private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);

    public PaymentService(PaymentRepository paymentRepository, PaymentMapper paymentMapper) {
        this.paymentRepository = paymentRepository;
        this.paymentMapper = paymentMapper;
    }

    public PaymentDTO get(int id) {
        Optional<Payment> optionalPayment = this.paymentRepository.findById(id);
        return this.paymentMapper.toDTO(optionalPayment.orElse(null));
    }

    public PaymentDTO create(PaymentDTO paymentDTO) {
        // Log the input PaymentDTO
        logger.debug("Received PaymentDTO: {}", paymentDTO);

        // Convert PaymentDTO to Payment entity
        Payment payment = this.paymentMapper.toPayment(paymentDTO);
        // Log the Payment object before saving
        logger.debug("Mapped Payment entity before saving: {}", payment);

        // Save the Payment entity
        Payment savedPayment = this.paymentRepository.save(payment);

        // Log the saved Payment object
        logger.debug("Saved payment: {}", savedPayment);

        // Convert saved Payment entity to PaymentDTO and return
        return this.paymentMapper.toDTO(savedPayment);
    }


    public PaymentDTO getPaymentByCode(String paymentCode) {
        Optional<Payment> optionalPayment = this.paymentRepository.findByPaymentCode(paymentCode);
        return this.paymentMapper.toDTO(optionalPayment.orElse(null));
    }

    public PaymentDTO getPaymentByPurchaseCode(String purchaseCode) {
        Optional<Payment> optionalPayment = this.paymentRepository.findByPurchaseCode(purchaseCode);
        return this.paymentMapper.toDTO(optionalPayment.orElse(null));
    }
}
