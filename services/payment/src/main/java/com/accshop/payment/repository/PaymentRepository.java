package com.accshop.payment.repository;

import com.accshop.payment.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {
    Optional<Payment> findByPaymentCode(String paymentCode);

    Optional<Payment> findByPurchaseCode(String purchaseCode);
}
