CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE payment(
    payment_id SERIAL PRIMARY KEY,
    payment_code VARCHAR(50) UNIQUE DEFAULT ('PAYM-' || uuid_generate_v4()::TEXT),
    purchase_code VARCHAR(50) NOT NULL UNIQUE,
    amount DECIMAL(10,2) NOT NULL,
    method VARCHAR(50),
    status VARCHAR(50) DEFAULT 'PENDING',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT chk_status CHECK (status IN ('PENDING','COMPLETED','FAILED','CANCELED','PROCESSING','ONHOLD','DECLINED'))
)

-- Create the sequence for the payment_id
CREATE SEQUENCE IF NOT EXISTS payment_id_seq START 1;

-- Create the payment table
CREATE TABLE IF NOT EXISTS payment (
    payment_id BIGINT DEFAULT nextval('payment_id_seq') PRIMARY KEY,
    payment_code VARCHAR(50) UNIQUE DEFAULT ('PAYM-' || uuid_generate_v4()::TEXT),
    purchase_code VARCHAR(50),
    customer_code VARCHAR(50),
    payment_status VARCHAR(50),
    amount NUMERIC(19, 2),
    transaction_id VARCHAR(100) UNIQUE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    metadata JSONB,
    CONSTRAINT chk_amount CHECK (amount >= 0),
    CONSTRAINT chk_status CHECK (payment_status IN ('PENDING', 'COMPLETED', 'FAILED'))
);

CREATE OR REPLACE FUNCTION set_payment_code()
RETURNS TRIGGER AS $$
BEGIN
    IF NEW.payment_code IS NULL THEN
        NEW.payment_code := 'PAYM-' || uuid_generate_v4();
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER before_insert_payment
BEFORE INSERT ON payment
FOR EACH ROW
EXECUTE FUNCTION set_payment_code();
