# AccShop

AccShop is a backend for an e-commerce store built to showcase
software development practices using Spring Boot and 
microservices.
This project includes two microservices, 
inter-service communication, and database integration 
with containerized PostgreSQL databases using Docker.

## Table of Contents

- [Project Structure](#project-structure)
- [Technologies Used](#technologies-used)
- [Setup and Installation](#setup-and-installation)
- [Docker Configuration](#docker-configuration)
- [Microservices](#microservices)
- [Endpoints](#endpoints)
- [Inter-Service Communication](#inter-service-communication)
- [Future Enhancements](#future-enhancements)
- [License](#license)

## Project Structure Overview

- **accshop/**
    - **services/**
        - **store/**
          - **src/main/java/com/accshop/store/**
            - **entity/**
                - Address.java
                - Cart.java
                - CartItem.java
                - Category.java
                - Customer.java
                - Inventory.java
                - Order.java
                - Product.java
                - ProductCategory.java
            - **controller/**
            - **service/**
            - **repository/**
    - **payment/**
      - **src/main/java/com/accshop/payment/**
        - **entity/**
          - Payment.java
        - **controller/**
        - **service/**
        - **repository/**
    - docker-compose.yml


## Technologies Used

- Java 21
- Spring Boot
- Docker
- PostgreSQL
- PgAdmin
- Apache Kafka (planned)
- Maven

## Setup and Installation

### Prerequisites

- Java 21
- Docker
- Docker Compose
- Maven

### Installation

1. **Clone the repository:**
   ```sh
   git clone https://github.com/idrissmouliom/accshop.git
   cd accshop
   ```

2. **Build the project using Maven:**

    ```sh
    mvn clean install
    ```
  
3. **Start Docker containers:**
   ```sh
    docker-compose up -d
    ```
## Docker Configuration:
  This project uses Docker to manage
  PostgreSQL and PgAdmin services. 
  Find the **docker-compose.yml** configuration file
  in the repository.

## Microservices
### Store Microservice
The store microservice manages the following entities:

- Customer
- Address
- Order
- Cart
- CartItem
- Product
- Category
- Inventory
- ProductCategory

### Payment Microservice
The payment microservice manages the ***Payment*** entity.

## Endpoints
Endpoints will be implemented to handle CRUD 
operations for the above entities. 
Examples include:

- /api/customers
- /api/orders
- /api/products
- /api/payments

## Inter-Service Communication
### Phase 1: HTTP Requests
Initially, inter-service communication will be 
handled using HTTP requests. Services will
communicate via RESTful APIs to perform 
necessary operations.

### Phase 2: Kafka
In the second phase, Apache Kafka will be 
introduced to handle asynchronous communication 
between services, improving scalability and 
reliability.

## Future Enhancements
- Implement Apache Kafka for asynchronous 
communication between microservices.
- Add more endpoints and functionalities.
- Enhance security with Spring Security.
- Add unit and integration tests.

## License
This project is licensed under the MIT License.
See the LICENSE file for details.
